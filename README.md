# Link_fix 
> **Initial target : updating anti-distractor of D(R)ANE for hypertext links to Youtube videos in LibreOffice docs**

The script modify any hypertext link in OpenDocument files (e.g. LibreOffice .odt .ods .odp, and hopefully in .docx Microsoft(r) Word files), based on a given "root" (left part of the link) and its replacement.

Indeed, "antidistractor" of D(R)ANE for YT videos has changed its URL. So links are not working anymore (2022-08-28).    
The script intends to fix it by moving `https://dane.web.ac-grenoble.fr/article/couper-et-distribuer-des-videos-sans-distracteur...` to the new equivalent `https://dane.web.ac-grenoble.fr/outils-numeriques-0/decoupeur-de-video...`.

> Notice that it is a **quick and dirty project** writen to solve a particular issue and that **I don't intend to develop and improve it seriously**, so be careful using it...    
> E.g. unfortunately **it seems buggy on Windows** (*encoding issue ? I don't have time to explore it nore this OS on my computer...*)

## Command and use cases

> following `<cmd>` could means, depending on your OS and parameters,
> * On UNIX-like (Linux, MacOS...): `./link_fit.py` if it is set as executable (`chmod +x link_fit.py`), else `python3 link_fit.py` (or with `python`)
> * On Windows, something like `py link_fit.py` (or with `py3` ? `python` ? `python3` ? `python.exe` ??)

Some optional parameters can be used. They must take place just after `<cmd>` on command line:

* **Without `-r`** option, **file name(s)** have to be indicated `<cmd> file1.odt file2.odt other*.odt` (*wildcard accepted*)
* **With `-r`** the script fixes any file in the given directory and its subdirectories (recursively) if they have one of the following **extension**. `<cmd> -r . odt docx`  (*where `.` means "the current directory"*) or `<cmd> my_sub_dir odt`...
* Both case can be combined with **`-B` option to avoid the creation of backup files** of original versions (by default with no `-B`, this backup file is writen).

## License

This script is provided without any guarantee, under GPL 3.0 (or above) licence, https://www.gnu.org/licenses/gpl-3.0.html 

