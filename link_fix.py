#!/usr/bin/env python3

"""Replace the root (left part) of hypertext links in OpenDocument files (LibreOffice...)
(and hopefully in .docx Open XML Microsoft(r) text files).

E.g. D(R)ANE former "anti-distracteur" links (by default).

Distributed without any guarantee under GPL 3.0 license https://www.gnu.org/licenses/gpl-3.0.html
by Jean-Marc GERVAIS gervaisprof[at]free.fr
"""

import os, shutil
from zipfile import ZipFile, ZIP_DEFLATED
from tempfile import mkdtemp

WRONG_LINK = "https://dane.web.ac-grenoble.fr/article/couper-et-distribuer-des-videos-sans-distracteur"
# https://s.42l.fr/ could be useful too, as it was a short for the previous URL...

NEW_LINK = "https://dane.web.ac-grenoble.fr/outils-numeriques-0/decoupeur-de-video"
BACKUP_SUFFIX = ".BACKUP"

def link_fix(document, wrong=WRONG_LINK, new=NEW_LINK, backup=True):
    """Modify link in one document.
    
    `wrong` is the left part of the hypertext link to modify, `new` the correct one.
    A backup of the original file is done if `backup` is `True` (default).
    
    Return True if a modification has occurred, False otherwise.
    """
    prefix = 'xlink:href="'  # OpenDocuments .odt .ods .odp
    crosoft_prefix = "HYPERLINK &quot;"
    temp_dir = mkdtemp()
    
    # Extract internal files
    with ZipFile(document, 'r') as zip_object:
        file_names = zip_object.namelist()
        for file_name in file_names:
            file_copy = zip_object.extract(file_name, temp_dir)
            
            # Modify links if needed
            if file_name == "content.xml":
                with open(file_copy, "r") as content_xml:
                    xml = content_xml.read()
                new_xml = xml.replace(prefix + wrong, 
                                      prefix + new)
                # Nothing to fix ?
                if xml == new_xml:
                    return False
                    
                # Rewrite fixed content.xml
                with open(file_copy, "w") as new_content_xml:                                  
                    new_content_xml.write(new_xml)
                    
            elif file_name == "word/document.xml":
                with open(file_copy, "r") as docu_xml:
                    xml = docu_xml.read()
                new_xml = xml.replace(crosoft_prefix + wrong, 
                                      crosoft_prefix + new)
                # Nothing to fix ?
                if xml == new_xml:
                    return False
                    
                # Rewrite fixed word/document.xml
                with open(file_copy, "w") as new_content_xml:                                  
                    new_content_xml.write(new_xml)
           

    # Rewrite fixed document
    if backup:
        shutil.copy2(src = document,
                     dst = document + BACKUP_SUFFIX)
    os.remove(document)   
    
    with ZipFile(document, 'x') as new_doc:
        for file in file_names:
            new_doc.write(os.path.join(temp_dir, file),
                          arcname=file,
                          compress_type=ZIP_DEFLATED)
    return True
        

def all_files(root_dir='.', extension=".odt"):
    """Return recursively all relative file paths, including in subdirectories.
    
    Only files with the correct extension are listed.
    """    
    files_list = []
    if extension[0] != '.':
        extension = '.' + extension
    for here, dirs, files in os.walk(root_dir):
        for f in files:
            if f.endswith(extension):
                files_list.append(os.path.join(here, f))
        for d in dirs:
            if d == []: 
                files_list.extend(all_files(os.path.join(here, d)))
    return files_list

if __name__ == "__main__":
    
    import sys
    args = sys.argv[1:]

    # No args => help   
    if len(args) == 0:
        print("===== Usage =====\nOptional parameters first")
        print("* `-B` to avoid the (default) creation of backup of original files\n\n* With or without `-B`:")
        print("  .  `-r`  + directory to explore + accepted extensions (`.odt` by default)")
        print("  or\n  .  (without `-r`) file(s) to explore (wildcards like `*` accepted)\n")
        print("===== Examples =====")
        print("(`<cmd>` means, depending on your OS:")
        print(" `python3 link_fix.py`   or `python link_fix.py`  `./link_fix.py` (if marked as executable)")
        print(" or, on Windows(r), something like `python.exe link_fix.py`) \n")
        print("`<cmd> my_file1.odt my_file2.odt`")
        print("`<cmd> my_file*.odt`")
        print("`<cmd> -B my_file*.odt`")
        print("`<cmd> -r . odt odp`")
        print("`<cmd> -B -r directory_to_explore odt odp ods`")
        exit()
    
    # Options
    backup = True   
    recursive = False
    while len(args) > 0 and args[0] in ("-B", "-r"):
        first_arg = args[0]
        if first_arg in ("-B", "-r"):   
            args = args[1:]
        if first_arg == "-B":
            backup = False
        if first_arg == '-r':
            recursive = True
    
    if len(args) < 1:
            print(" /!\ \tArguments are missing... Aborted")
            exit()
    
    # Recursive => files listed from extension(s), not given directly
    if recursive:
        if len(args) < 2:
            print(" /!\ \tPlease give the directory to explore, then the extension(s), e.g. `.odt`... Aborted")
            exit()
        extensions = args[1:]
        files = []
        for ext in extensions:
            files.extend(all_files(root_dir=args[0], extension=ext))
    else:
        files = args
                         
    nb_files = len(files)
    nb_fixed = 0

    # Fixing hypertext links
    for file in files:
        # Already fixed ?
        if os.path.basename(file).endswith(BACKUP_SUFFIX):
            nb_files -= 1
            continue
        try:
            modified = link_fix(file, backup=backup)
            if modified:
                nb_fixed += 1
                print(f" . '{file}' fixed")
            else:
                print(f" . '{file}' unchanged")
        except IsADirectoryError:  # e.g. with wildcard
            nb_files -= 1
        except Exception as err:
            modified = False
            print(f" /!\ \tImpossible to modify `{file}`.")
            print(f"     \tException = `{err.filename}` {err.strerror}")
        
    print(f"{nb_fixed} files out of {nb_files} were fixed.")
